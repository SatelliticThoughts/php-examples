<?php
	class Model
	{
		private $data;
		
		public function __construct()
		{
			$this->set_data("This is data");
		}
		
		public function get_data()
		{
			return $this->data;
		}
		
		public function set_data($data)
		{
			$this->data = $data;
		}
	}
	
	class View
	{
		private $model;
		private $controller;
		
		public function __construct($model, $controller)
		{
			$this->model = $model;
			$this->controller = $controller;
		}
		
		public function output()
		{
			return "<p>" . $this->model->get_data() . "</p>";
		}
	}
	
	class Controller
	{
		private $model;
		
		public function __construct($model)
		{
			$this->model = $model;
		}
	}
	
	$model = new Model();
	$controller = new Controller($model);
	$view = new View($model, $controller);
	
	echo $view->output();
	echo "\n";
?>
