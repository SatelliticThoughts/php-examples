<?php
class Person
{
	var $name;
	var $age;

	function __construct($name, $age)
	{
		$this->setName($name);
		$this->setAge($age);
	}

	function setName($name)
	{
		$this->name = $name;
	}

	function getName()
	{
		return $this->name;
	}

	function setAge($age)
	{
		$this->age = $age;
	}

	function getAge()
	{
		return $this->age;
	}
}

$person1 = new Person("Bob", 99);
$person2 = new Person("Bobby", 31);

echo "Person: $person1->name; $person1->age";
echo "\nPerson: $person2->name; $person2->age";

?>

