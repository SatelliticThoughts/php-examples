<?php
$x = 14;
$y = 5;

echo "Integer Calculations, x = $x; y = $y";

echo "\nx + y = ", $x + $y;
echo "\nx - y = ", $x - $y;
echo "\nx * y = ", $x * $y;
echo "\nx / y = ", $x / $y;
echo "\nx % y = ", $x % $y;

$x = 14.23;
$y = 5.89;

echo "\n\nFloating point Calculations, x = $x; y = $y";

echo "\nx + y = ", $x + $y;
echo "\nx - y = ", $x - $y;
echo "\nx * y = ", $x * $y;
echo "\nx / y = ", $x / $y;
echo "\nx % y = ", $x % $y;

echo "\n\nString Manipulation";

$hello = "hello ";
echo "\n", $hello;

$hello .= "World!";
echo "\n", $hello;

echo "\n", strlen($hello);
echo "\n", substr($hello, 3, 2);

$pos = strpos($hello, "dog");
if ($pos == false) 
{
	echo "\nSearch Term not in string";
}
else
{
	echo "\nSearch Term is in string";
}

echo "\n\nArrays\n";

$numbers = [2,1,5,3,6,7,6];
foreach ($numbers as $number) 
{
	echo $number, " ";
}

echo "\n";
$numbers = array_merge($numbers, [2, 2, 2, 2, 2]);
foreach ($numbers as $number)
{
	echo $number, " "; 
}
?>

