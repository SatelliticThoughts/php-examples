<?php
function sayHello()
{
	echo "Hello\n";
}

function getWord()
{
	return "dog";
}

function doubleNumber($x)
{
	echo $x * 2, "\n";
}

function addNumbers($x, $y)
{
	return $x + $y;
}

sayHello();
echo getWord(), "\n";
doubleNumber(5);
echo addNumbers(4, 5);
?>

